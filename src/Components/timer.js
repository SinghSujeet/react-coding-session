import React from "react";

export default function App() {
  const [time, setTime] = React.useState(0);
  React.useEffect(() => {
    setInterval(() => {
        setTime(time + 1);
        console.log(time)
    }, 1000);
      
      return (() => {
          clearInterval()
      })
  }, [time]);
  return (
    <div className="App">
      <h1>Hello CodeSandbox</h1>
      <h2>Start editing to see some magic happen!</h2>
      <h2>{time} seconds has passed</h2>
    </div>
  );
}
