import React from "react";
import "./style.css";
import AddTodo from "./AddTodo";
import TodoList from "./TodoList";
import VisibilityFilters from "./VisibilityFilters";
import Timer from "../Components/timer"
import Demo from "./Demo"
export default function TodoApp() {
  return (
    <div className="todo-app">
      <h1>Todo List</h1>
      <AddTodo />
       <VisibilityFilters />
      <TodoList />     
      <Demo/>
    </div>
  );
}
