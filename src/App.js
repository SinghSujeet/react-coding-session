import logo from './logo.svg';
import './App.css';
import { Provider } from "react-redux";
import store from "../src/redux/store";
import TodoApp from "../src/Components/TodoApp"
function App() {
  return (
    <Provider store={store}>
    <TodoApp />
  </Provider>
  );
}

export default App;
